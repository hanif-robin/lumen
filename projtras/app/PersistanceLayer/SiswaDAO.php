<?php


namespace App\PersistanceLayer;


class SiswaDAO extends GenericDAO
{
    protected $table = "siswa";
    protected $primariKey = "id";
    public $timestamps = false;
}
