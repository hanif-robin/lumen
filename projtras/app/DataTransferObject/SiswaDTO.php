<?php
namespace App\DataTransferObject;

class SiswaDTO extends GenericDTO
{
    private $id;
    private $nama;
    private $nis;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getNama()
    {
        return $this->nama;
    }

    public function setNama($nama): void
    {
        $this->nama = $nama;
    }

    public function getNis()
    {
        return $this->nis;
    }

    public function setNis($nis): void
    {
        $this->nis = $nis;
    }
}
