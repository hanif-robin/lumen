<?php
namespace App\BusinessLayer;
use App\DataTransferObject\SiswaDTO;
use App\PersistanceLayer\SiswaDAO;
use App\PresentationLayer\ResponseCreatorPresentationLayer;

class SiswaBusinessLayer extends GenericBusinessLayer
{
    public function aksiAmbilSemua()
    {
        try {
            $data = SiswaDAO::select('nama', 'nis')
                ->orderBy('nama', 'ASC')
                ->get();
            if (count($data) == 0) {
                $response = new ResponseCreatorPresentationLayer(404, 'Data siswa tidak ditemukan', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data berhasil ditemukan', null, []);
            return $response->getResponse();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null, $errors);
        }

        return $response->getResponse();
    }

    public function aksiAmbilBerdasarkanID(SiswaDTO $params)
    {
        try {
            $data = SiswaDAO::select('nama', 'nis')
                ->whereIn('id', $params->getId())
                ->first();
            if (is_null($data)) {
                $response = new ResponseCreatorPresentationLayer(404, 'Data siswa tidak ditemukan', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data berhasil ditemukan', null, []);
            return $response->getResponse();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null, $errors);
        }

        return $response->getResponse();
    }

    public function aksiSimpan(SiswaDTO $params)
    {
        try {
            $data = new SiswaDAO();
            $data->nama = $params->getNama();
            $data->nis = $params->getNis();
            $data->save();
            if (is_null($data)) {
                $response = new ResponseCreatorPresentationLayer(404, 'Data siswa tidak valid', null, []);
                return $response->getResponse();
            }

            $response = new ResponseCreatorPresentationLayer(200, 'Data berhasil disimpan', null, []);
            return $response->getResponse();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $response = new ResponseCreatorPresentationLayer(500, 'Server sedang dalam perbaikan', null, $errors);
        }

        return $response->getResponse();
    }
}
