<?php
namespace App\Http\Controllers;
use App\BusinessLayer\SiswaBusinessLayer;
use App\DataTransferObject\SiswaDTO;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    private $siswaBusinessLayer;

    public function __construct()
    {
        $this->siswaBusinessLayer = new SiswaBusinessLayer();
    }

    public function ambilSemua()
    {
        $hasil = $this->siswaBusinessLayer->aksiAmbilSemua();
        return response()->json($hasil, $hasil['code']);
    }

    public function ambilBerdasarkanId(Request $request)
    {
        $data = new SiswaDTO();
        $data->setId($request->input('id'));
        
        $hasil = $this->siswaBusinessLayer->aksiAmbilBerdasarkanID($data);
        return response()->json($hasil, $hasil['code']);
    }

    public function simpan(Request $request)
    {
        $data = new SiswaDTO();
        $data->setNama($request->input('nama'));
        $data->setNis($request->input('nis'));

        $hasil = $this->siswaBusinessLayer->aksiSimpan($data);
        return response()->json($hasil, $hasil['code']);
    }
}
